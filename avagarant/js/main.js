'use strict';

$(function () {

    // SVG IE11 support
    svg4everybody();

    $('.btn_modal').fancybox({
        'padding': 0
    });

    $('.nav_toggle').on('click', function (e) {
        e.preventDefault();
        $('.nav_toggle').toggleClass('open');
        $('.nav').toggleClass('open');
    });

    // Skicky Header

    let header = $('.nav');
    let $h = header.offset().top;

    $(window).scroll(function () {
        if ($(window).scrollTop() > $h) {
            header.addClass('nav_fix');
        } else {
            header.removeClass('nav_fix');
        }
    });

    $('.header__mobile_search').on('click', function (e) {
        e.preventDefault();
        $('.header').toggleClass('open_search');
    });

    let goodsSlider = new Swiper('.goods__slider', {
        loop: true,
        pagination: {
            el: '.swiper-pagination',
        },

        // Navigation arrows
        navigation: {
            nextEl: '.goods__nav_next',
            prevEl: '.goods__nav_prev',
        }
    });


    // Goods

    $('.goods-select__button').on('click', function (e) {
        e.preventDefault();
        $('.goods-select').toggleClass('open');
    });

    $('.goods_switch').on('click', function (e) {
        e.preventDefault();

        let goodsItemName = $(this).text();
        let goodsSlideNum = $(this).attr('data-value');

        $('.goods-select').removeClass('open');
        $('.goods_switch').removeClass('active');
        $('.goods_nav__hidden').removeClass('open');
        $(this).addClass('active');
        $('.goods-select__button_text').text(goodsItemName);
        goodsSlider.slideTo(goodsSlideNum, 600);
    });

    goodsSlider.on('slideChange', function () {
        let goodsSlideAmount = '.goods_switch_' + (goodsSlider.realIndex);
        let goodsSlideName = $('.goods').find(goodsSlideAmount).first().text();
        $('.goods_switch').removeClass('active');
        $('.goods').find(goodsSlideAmount).addClass('active');
        $('.goods-select__button_text').text(goodsSlideName);
    });

    // materials

    let materials = new Swiper('.materials__slider', {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 1,
        navigation: {
            nextEl: '.materials__arrow_next',
            prevEl: '.materials__arrow_prev',
        },
        breakpoints: {
            768: {
                spaceBetween: 20,
                slidesPerView: 2
            },
            992: {
                spaceBetween: 20,
                slidesPerView: 3
            }
        }
    });

    $('.materials__select_button').on('click', function (e) {
        e.preventDefault();
        $('.materials__select').toggleClass('open');
    });

    $('.materials_filter').on('click', function (e) {
        e.preventDefault();
        let materialsItemName = $(this).text();
        let filter = $(this).attr('data-tab');

        $('.materials__select').removeClass('open');
        $('.materials__select_button span').text(materialsItemName);

        $('.materials_filter').removeClass('active');
        $(this).addClass('active');

        if (filter == 'all') {
            $("[data-filter]").removeClass("non-swiper-slide").addClass("swiper-slide").show();
            materials.destroy();
            materials = new Swiper('.materials__slider', {
                loop: true,
                spaceBetween: 10,
                slidesPerView: 1,
                navigation: {
                    nextEl: '.materials__arrow_next',
                    prevEl: '.materials__arrow_prev',
                },
                breakpoints: {
                    768: {
                        spaceBetween: 20,
                        slidesPerView: 2
                    },
                    992: {
                        spaceBetween: 20,
                        slidesPerView: 3
                    }
                }
            });
        }
        else {
            $(".materials__slider .swiper-slide").not("[data-filter='" + filter + "']").addClass("non-swiper-slide").removeClass("swiper-slide").hide();
            $("[data-filter='" + filter + "']").removeClass("non-swiper-slide").addClass("swiper-slide").attr("style", null).show();
            materials.destroy();
            materials = new Swiper('.materials__slider', {
                loop: true,
                spaceBetween: 10,
                slidesPerView: 1,
                navigation: {
                    nextEl: '.materials__arrow_next',
                    prevEl: '.materials__arrow_prev',
                },
                breakpoints: {
                    768: {
                        spaceBetween: 20,
                        slidesPerView: 2
                    },
                    992: {
                        spaceBetween: 20,
                        slidesPerView: 3
                    }
                }
            });
        }

    });


    $(".service__item")
        .mouseenter(function () {
            $(this).find('.service__item_text').slideDown('fast');
        })
        .mouseleave(function () {
            $(this).find('.service__item_text').slideUp('fast');
        });


    let work = new Swiper('.work__slider', {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 1,
        navigation: {
            nextEl: '.work__nav_next',
            prevEl: '.work__nav_prev',
        },
        breakpoints: {
            768: {
                spaceBetween: 20,
                slidesPerView: 2
            },
            992: {
                spaceBetween: 20,
                slidesPerView: 3
            },
            1200: {
                spaceBetween: 20,
                slidesPerView: 4
            }
        }
    });

    $('.footer__nav_title').on('click', function (e) {
        e.preventDefault();
        let winWight = $(window).width();

        if (winWight < 992) {
            $(this).closest('.footer__nav_group').find('.footer__nav_content').slideToggle('fast');
            $(this).toggleClass('open');
        }
        else {

        }
    });


    $(window).resize(function () {
        let wWidth = $(window).width();
        if (wWidth > 991) {
            $('.footer__nav_content').removeAttr('style');
        }
    });

    // Hide dropdown

    $('body').click(function (event) {

        if ($(event.target).closest(".goods-select").length === 0) {
            $(".goods-select").removeClass('open');
        }

        if ($(event.target).closest(".collapsible_toggle").length === 0) {
            $(".collapsible_hidden").removeClass('open');
        }

        if ($(event.target).closest(".materials__select").length === 0) {
            $(".materials__select").removeClass('open');
        }

        if ($(event.target).closest(".tabs__nav").length === 0) {
            $(".tabs__nav").removeClass('open');
        }

        if ($(event.target).closest(".sort").length === 0) {
            $(".sort").removeClass('open');
        }
    });


    function collapsibleNav(block) {

        let $btn = $(block).find('.collapsible_button');
        let $toggle = $(block).find('.collapsible_toggle');
        let $vlinks = $(block).find('.collapsible_links');
        let $hlinks = $(block).find('.collapsible_hidden');


        let numOfItems = 0;
        let totalSpace = 0;
        let closingTime = 1000;
        let breakWidths = [];

        // Get initial state
        $vlinks.children().outerWidth(function (i, w) {
            totalSpace += w;
            numOfItems += 1;
            breakWidths.push(totalSpace);
        });

        let availableSpace, numOfVisibleItems, requiredSpace, timer;

        function check() {

            // Get instant state
            availableSpace = $vlinks.width() - 10;
            numOfVisibleItems = $vlinks.children().length;
            requiredSpace = breakWidths[numOfVisibleItems - 1];

            // There is not enought space
            if (requiredSpace > availableSpace) {
                $vlinks.children().last().prependTo($hlinks);
                numOfVisibleItems -= 1;
                check();
                // There is more than enough space
            } else if (availableSpace > breakWidths[numOfVisibleItems]) {
                $hlinks.children().first().appendTo($vlinks);
                numOfVisibleItems += 1;
                check();
            }
            // Update the button accordingly
            $toggle.attr("count", numOfItems - numOfVisibleItems);
            if (numOfVisibleItems === numOfItems) {
                $toggle.addClass('hidden');
            } else $toggle.removeClass('hidden');
        }

        // Window listeners
        $(window).resize(function () {
            check();
        });

        $btn.click(function (e) {
            e.preventDefault();
            $hlinks.toggleClass('open');
        });

        check();
    }

    collapsibleNav('.goods_nav');
    collapsibleNav('.materials__nav');


    let gallerySlider = new Swiper('.product__media_gallery', {
        loop: false,
        spaceBetween: 10,
        breakpoints: {
            768: {
                spaceBetween: 20
            }
        }
    });

    gallerySlider.on('slideChange', function () {
        let currentSlide = '.item' + (gallerySlider.realIndex + 1);
        console.log(currentSlide);
        $('.product__media_thumbs').find('li').removeClass('active');
        $('.product__media_thumbs').find(currentSlide).closest('li').addClass('active');
    });


    $('.product__media_thumbs a').on('click', function (e) {
        e.preventDefault();
        let galleryImage = $(this).attr("data-image") - 1;

        $('.product__media_thumbs').find('li').removeClass('active');
        $(this).closest('li').addClass('active');
        gallerySlider.slideTo(galleryImage, 600);
    });

    let custom_slider = new Swiper('.product__custom_slider', {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 2,
        navigation: {
            nextEl: '.product__custom_next',
            prevEl: '.product__custom_prev',
        },
        breakpoints: {
            768: {
                spaceBetween: 20,
                slidesPerView: 3
            },
            992: {
                spaceBetween: 20,
                slidesPerView: 2
            },
            1200: {
                spaceBetween: 20,
                slidesPerView: 3
            }
        }
    });

    $('.tabs__nav_button').on('click', function (e) {
        e.preventDefault();
        $('.tabs__nav').toggleClass('open');
    });

    $('.tabs__nav a').on('click', function (e) {
        e.preventDefault();
        let tab = $($(this).attr("data-target"));
        let box = $(this).closest('.tabs');
        let tabName = $(this).text();

        $('.tabs__nav_button span').text(tabName);
        $(this).closest('.tabs__nav').find('a').removeClass('active');
        $(this).addClass('active');

        box.find('.tabs__item').removeClass('active');
        box.find(tab).addClass('active');

        $('.tabs__nav').removeClass('open');
    });

    $('.sort__button').on('click', function (e) {
        e.preventDefault();
        $('.sort').toggleClass('open');
    });


    $('.sort__dropdown a').on('click', function (e) {
        e.preventDefault();
        $('.sort').removeClass('open');
        $('.sort__button span').text($(this).text());
    });

    let otherSlider = new Swiper('.other__slider', {
        loop: true,
        spaceBetween: 20,
        slidesPerView: 1,
        navigation: {
            nextEl: '.other__arrow_next',
            prevEl: '.other__arrow_prev',
        },
        breakpoints: {
            768: {
                spaceBetween: 20,
                slidesPerView: 2
            },
            992: {
                spaceBetween: 20,
                slidesPerView: 3
            },
            1300: {
                spaceBetween: 20,
                slidesPerView: 4
            }
        }
    });

    let goodsBlock = new Swiper('.goods-block__slider', {
        loop: true,
        spaceBetween: 20,
        slidesPerView: 1,
        navigation: {
            nextEl: '.goods-block_next',
            prevEl: '.goods-block_prev',
        },
        breakpoints: {
            768: {
                spaceBetween: 20,
                slidesPerView: 3
            },
            992: {
                spaceBetween: 20,
                slidesPerView: 4
            },
            1300: {
                spaceBetween: 20,
                slidesPerView: 5
            }
        }
    });

    let winWidtn = $(window).width();

    if (winWidtn < 768) {

        $(".table_responsive").mCustomScrollbar({
            axis: "x",
            theme: "minimal-dark"
        });
    }
    else {

    }

    $(window).resize(function () {

        if (winWidtn < 768) {

            $(".table_responsive").mCustomScrollbar({
                axis: "x",
                theme: "minimal-dark"
            });
        }
        else {

        }

    });

    let videoBlock = new Swiper('.video-slider__main', {
        loop: true,
        spaceBetween: 0,
        slidesPerView: 1,
        slidesPerGroup: 1,
        pagination: {
            clickable: true,
            el: '.swiper-pagination',
        },
        breakpoints: {
            768: {
                spaceBetween: 0,
                slidesPerView: 2,
                slidesPerGroup: 2
            },
            992: {
                spaceBetween: 0,
                slidesPerView: 3,
                slidesPerGroup: 3
            }
        }
    });

    let cert = new Swiper('.cert', {
        loop: true,
        spaceBetween: 20,
        slidesPerView: 1,
        slidesPerGroup: 1,
        pagination: {
            clickable: true,
            el: '.swiper-pagination',
        },
        breakpoints: {
            768: {
                spaceBetween: 20,
                slidesPerView: 2,
                slidesPerGroup: 2
            },
            992: {
                spaceBetween: 20,
                slidesPerView: 3,
                slidesPerGroup: 3
            },
            1300: {
                spaceBetween: 20,
                slidesPerView: 4,
                slidesPerGroup: 4
            }
        }

    });


    let baseGallery = new Swiper('.base__gallery_slider', {
        loop: false,
        spaceBetween: 15,
        slidesPerView: 1,
        breakpoints: {
            768: {
                spaceBetween: 20,
                slidesPerView: 2,
            }
        }

    });

});




