let sourceDir   = '#src';
let projectDir  = 'avagarant';

let online      = true;

let paths = {

    html: {
        src     : [sourceDir + '/*.html', '!' + sourceDir + '/_*.html'],
        dest    : projectDir + '/',
    },

    styles: {
        src     : sourceDir + '/scss/main.scss',
        dest    : projectDir + '/css/',
    },

    scripts: {
        src     : sourceDir + '/js/main.js',
        dest    : projectDir + '/js/',
    },

    fonts: {
        src     : sourceDir + '/fonts/**/*.*',
        dest    : projectDir + '/fonts/',
    },

    lib: {
        js      : [
            '#src/js/libs/jquery.min.js',
            '#src/js/libs/swiper/swiper-bundle.min.js',
            '#src/js/libs/jquery.mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js',
            '#src/js/libs/svg4everybody.legacy.min.js',
            '#src/js/libs/fancybox/jquery.fancybox.min.js'
        ],
        css     : [
            'node_modules/font-awesome/css/font-awesome.min.css',
            '#src/js/libs/swiper/swiper-bundle.min.css',
            '#src/js/libs/jquery.mCustomScrollbar/jquery.mCustomScrollbar.min.css',
            '#src/js/libs/fancybox/jquery.fancybox.min.css'

        ]
    },

    images: {
        src:  [sourceDir + '/img/**/*.{jpg,png,svg,gif,ico,webp}', '!' + sourceDir + '/img/sprite_icons/', '!' + sourceDir + '/img/sprite_icons.svg'],
        dest: projectDir + '/img/',
    },

    watch: {
        html    : sourceDir + '/**/*.html',
        css     : sourceDir + '/scss/**/*.scss',
        js      : sourceDir + '/js/**/*.js',
        img     : sourceDir + '/img/**/*.{jpg,png,svg,gif,ico,webp}'
    },

    deploy: {
        hostname:    'project.itdv.pro', // Deploy hostname
        destination: '/public_shtml/2020/avagarant', // Deploy destination
        include:     [/* '*.htaccess' */], // Included files to deploy
        exclude:     [ '**/Thumbs.db', '**/*.DS_Store' ], // Excluded files from deploy
    },

    clean: './' +  projectDir + '/'
};


let { src, dest }   = require('gulp');
let gulp            = require('gulp'),
    browsersync     = require('browser-sync').create(),
    del             = require('del'),
    fileinclude     = require('gulp-file-include'),
    scss            = require('gulp-sass'),
    autoprefixer    = require('gulp-autoprefixer'),
    group_media     = require('gulp-group-css-media-queries'),
    clean_css       = require('gulp-clean-css'),
    rename          = require('gulp-rename'),
    imagemin        = require('gulp-imagemin'),
    uglify          = require('gulp-uglify-es').default,
    concat          = require('gulp-concat'),
    svgSprite       = require('gulp-svg-sprite');

function browserSync(params) {
    browsersync.init({
        server: {
            baseDir: './' +  projectDir + '/'
        },
        notify: false,
        online: true
    })
}

function html() {
    return src(paths.html.src)
        .pipe(fileinclude())
        .pipe(dest(paths.html.dest))
        .pipe(browsersync.stream())
}

function styles() {
    return src(paths.styles.src)
        .pipe(
            scss({
                outputStyle: 'expanded'
            })
        )
        .pipe(
            group_media()
        )
        .pipe(
            autoprefixer({
                overrideBrowserslist: ['last 5 versions'],
                cascade: true
            })
        )
        .pipe(dest(paths.styles.dest))
        .pipe(clean_css())
        .pipe(
            rename({
                extname: '.min.css'
            })
        )
        .pipe(dest(paths.styles.dest))
        .pipe(browsersync.stream())
}

function scripts() {
    return src(paths.scripts.src)
        .pipe(dest(paths.scripts.dest))
        .pipe(
            uglify()
        )
        .pipe(
            rename({
                extname: '.min.js'
            })
        )
        .pipe(dest(paths.scripts.dest))
        .pipe(browsersync.stream())
}

function images() {
    return src(paths.images.src)
        .pipe(
            imagemin({
                progressive: true,
                svgPlugins: false,
                interlaced: true,
                optimizationLevel: 3
            })
        )
        .pipe(dest(paths.images.dest))
        .pipe(browsersync.stream())
}

function fonts() {
    return src(paths.fonts.src)
        .pipe(dest(paths.fonts.dest))
        .pipe(browsersync.stream())
}

function watchFiles(params) {
    gulp.watch([paths.watch.html],html);
    gulp.watch([paths.watch.css],styles);
    gulp.watch([paths.watch.js],scripts);
    gulp.watch([paths.watch.img],images);

}

function lib() {
    return src(sourceDir + '/js/libs/**/*.*')
        .pipe(dest(paths.scripts.dest + 'libs/'))
        .pipe(browsersync.stream())
}

function libJs() {
    return src(paths.lib.js)
        .pipe(concat('libs.min.js'))
        .pipe(dest(paths.scripts.dest))
        .pipe(browsersync.stream())
}

function libCss() {
    return src(paths.lib.css)
        .pipe(concat('libs.min.css'))
        .pipe(dest(paths.styles.dest))
        .pipe(browsersync.stream())
}

function spriteIcons() {
    return src(sourceDir + '/img/sprite_icons.svg')
        .pipe(dest(paths.images.dest))
        .pipe(browsersync.stream())
}

function clean(params) {
    return del(paths.clean);
}



let build =  gulp.series(clean, gulp.parallel(html, styles, scripts, images, lib, libJs, libCss, fonts, spriteIcons));
let watch =  gulp.parallel(build, watchFiles, browserSync);

exports.html            = html;
exports.fonts           = fonts;
exports.scripts         = scripts;
exports.styles          = styles;
exports.images          = images;
exports.lib             = lib;
exports.libJs           = libJs;
exports.libCss          = libCss;
exports.spriteIcons     = spriteIcons;
exports.clean           = clean;
exports.build           = build;
exports.watch           = watch;
exports.default         = watch;

